# The COMPAS Project, Phase 2: cata-condensed Hetero-Polycyclic Aromatic Systems

## Summary

This directory contains the COMPAS-2x and COMPAS-2D data sets, code, and auxiliary data associated with the expansion of the COMPAS Project to the chemical space of cata-condensed polyheterocyclic aromatic systems. The code is provided "as is". Minor edits may be required to tailor the scripts for different computational systems.


## Content
|  Data column header        | Data Set       |               | Description                                      |
|----------------------------|----------------|---------------|--------------------------------------------------| 
|                            | COMPAS-2x      | COMPAS-2D     |                                                  |  
|   name                     |    ✅         |    ✅         |    9-character alpha-numeric name                |   
|   formula                  |    ✅         |    ✅         |    Molecular formula                             |   
|   inchi                    |    ✅         |    ✅         |    InChI descriptor                              |   
|   smiles                   |    ✅         |    ✅         |    SMILES descriptor                             |   
|   charge                   |    ✅         |    ✅         |    Charge of the molecule                        |   
|   occupancy                |    ✅         |    ✅         |    Number of occupied molecular orbitals         |   
|   orbital_energies         |    ✅         |    ✅         |    Molecular orbital energies                    |   
|   homo                     |    ✅         |    ✅         |    Energy of the HOMO (eV)                       |   
|   lumo                     |    ✅         |    ✅         |    Energy of the LUMO (eV)                       |   
|   lumo+1                   |    ✅         |    ✅         |    Energy of the LUMO+1 (eV)                     |   
|   homo-1                   |    ✅         |    ✅         |    Energy of the HOMO-1 (eV)                     |   
|   gap                      |    ✅         |    ✅         |    HOMO-LUMO gap (eV)                            |   
|   electronic_energy        |    ✅         |    ✅         |    Final electronic energy from SCF (Eh)         |   
|   energy                   |    ✅         |    ✅         |    E_tot of minimum energy structure (Eh)        |   
|   dispersion               |    ✅         |    ✅         |    Dispersion (Eh)                               |   
|   ionization_potential     |    ✅         |    ✅         |    Adiabatic Electron affinity corrected (Eh)    |   
|   electron_affinity        |    ✅         |    ✅         |    Adiabatic electron affinity (Eh)              |   
|   nfod                     |    ✅         |    ❌         |    Fractional occupation density              |
|   nrings                   |    ✅         |    ❌         |    Number of rings                               |   
|   naromaticrings           |    ✅         |    ❌         |    Number of aromatic rings                      |   
|   natoms                   |    ✅         |    ❌         |    Total number of atoms                         |   
|   nheteroatoms             |    ✅         |    ❌         |    Number of heteroatoms                         |   
|   nheterocycles            |    ✅         |    ❌         |    Number of heterocycles                        |   
|   nbranch                  |    ✅         |    ❌         |    Number of branches                            |   
|   cyclobutadiene           |    ✅         |    ✅         |    Number of cyclobutadiene                      |   
|   pyrrole                  |    ✅         |    ✅         |    Number of pyrrole                             |   
|   borole                   |    ✅         |    ✅         |    Number of borole                              |   
|   furan                    |    ✅         |    ✅         |    Number of furan                               |   
|   thiophene                |    ✅         |    ✅         |    Number of thiophene                           |   
|   dhdiborine               |    ✅         |    ✅         |    Number of dhdiborine                          |   
|   14diborine               |    ✅         |    ✅         |    Number of 14diborine                          |   
|   pyrazine                 |    ✅         |    ✅         |    Number of pyrazine                            |   
|   pyridine                 |    ✅         |    ✅         |    Number of pyridine                            |   
|   borinine                 |    ✅         |    ✅         |    Number of borinine                            |   
|   benzene                  |    ✅         |    ✅         |    Number of benzene                             |   
|   h                        |    ✅         |    ✅         |    Number of hydrogen                            |   
|   c                        |    ✅         |    ✅         |    Number of carbons                             |   
|   b                        |    ✅         |    ✅         |    Number of borons                              |   
|   s                        |    ✅         |    ✅         |    Number of sulfurs                             |   
|   o                        |    ✅         |    ✅         |    Number of oxygens                             |   
|   n                        |    ✅         |    ✅         |    Number of nitrogens                           |   
|   aea_corr                 |    ✅         |    ❌         |    xTB-to-DFT-corrected Electron affinity (eV)   |   
|   aip_corr                 |    ✅         |    ❌         |    xTB-to-DFT-corrected Ionization potential (eV)|   
|   homo_corr                |    ✅         |    ❌         |    xTB-to-DFT-corrected HOMO affinity (eV)       |   
|   lumo_corr                |    ✅         |    ❌         |    xTB-to-DFT-corrected LUMO affinity (eV)       |   
|   gap_corr                 |    ✅         |    ❌         |    xTB-to-DFT-corrected HOMO/LUMO GAP (eV)       |   
|   energy_corr              |    ✅         |    ❌         |    xTB-to-DFT-corrected E_tot (Eh)               |
|   rmsd                     |    ❌         |    ✅         |    RMSD between xTB and DFT structures (Å)       |


## Folder structure

```
├── compas-2D.csv
├── compas-2D.parquet
├── compas-2D.sdf.gz
├── compas-2x.csv
├── compas-2x.parquet
├── compas-2x.sdf.gz
│
└── notebooks
    ├── data
    │   ├── benchmark
    │   └── test_data
    ├── figures
    ├── 00_benchmark.ipynb
    ├── 01_library_enumeration.ipynb
    ├── 02_geometry_generation.ipynb
    ├── 03_dft_calculation.ipynb
    ├── 04_model_selection.ipynb
    ├── 05_xtb_correction.ipynb
    ├── 06_data_exploration.ipynb
    └── utils

```
The `compas-2x.csv` and `compas-2D.csv` files contain the electronic properties of the molecules.
The `compas-2x.parquet` and `compas-2D.parquet` files contain the electronic properties of the molecules.
The `compas-2x.sdf.gz` and `compas-2D.sdf.gz` files contain the optimized geometries of the molecules, in the respective levels of theory.

### Notebooks

The Notebooks folder contains notebooks that provide walkthroughs for generating the data sets, selecting the model for the correction of the xTB-calculated properties, and generating figures (as in the accompanying paper). 

Both `environment.yml` and `requirements.txt` files are provided to install the required packages to run the notebooks, with either conda or pip. Additionally, a list of the required packages is provided below.
```
  # python
  - python=3.8.15
  # chemistry data handlers
  - rdkit=2022.09.4
  - mols2grid
  # data handlers
  - numpy
  - pandas
  - scikit-learn
  - scipy
  # ide
  - jupyter
  # data visualization
  - seaborn
  - matplotlib
  # xtb
  - xtb=6.5.1
```
#### Benchmarking

The Benchmarking folder contains the data used for the benchmark of the semiempirical methods and pickle files of the COMPAS-2x and COMPAS-2D data sets. 
#### Test Data

The `test_data` facilitating the execution of Jupyter notebooks without running any computationally expensive calculation. It contains a selection of 10 hPAHs (heterocyclic aromatic hydrocarbons).

## Authors and acknowledgment
The COMPAS database was conceptualized by Prof. Renana Gershoni-Poranne and is constructed under her supervision. The following people contributed to data generation, curation, and organization: 
1. Eduardo Mayo (Technion)
2. Dr. Sabyasachi Chakraborty (Technion)

The invaluable assistance of the following people is gratefully acknowledged: Prof. Dr. Peter Chen, Dr. Alexandra Tsybizova, and Dr. Alexandra Wahab.

In addition, the financial support of the Branco Weiss Fellowship is acknowledged.

## License
The COMPAS database is provided free-of-charge and is intended to be a resource for the scientific community.
It is licensed under a CC-BY-NC-SA license. 
