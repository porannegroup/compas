import seaborn as sns
from matplotlib import pyplot as plt
from pathlib import Path
import numpy as np

from matplotlib.ticker import MaxNLocator

PROPS = ['aea','aip','homo', 'lumo', 'gap','energy']
AXIS_LIMITS = {'aip':[[4, 8],[10, 14]],
               'ionization_potential':[[4, 8],[10, 14]],
                'aea':[[-4, 1],[-9, -5]],
                'electron_affinity':[[-4, 1],[-9, -5]],
                'homo':[[-8,-4],[-12,-9]],
                'lumo':[[-4,0],[-11,-7]],
                'gap':[[2,7],[-0,3]],
                'energy':[[-3000,-500],[-115,-30]]}
        
def contour_plot(data, prop, linewidths=None, tresh=0, name='contour-plot',levels=10, ax=None, limits=True):
    
    fig, ax_out = plt.subplots(figsize=(4,4),dpi=300)
    if linewidths:
        sns.kdeplot(data=data, x=f"{prop}_dft", y=f"{prop}_xtb", color='#212121', fill=False,levels=10, linewidths=linewidths,ax=ax)
        sns.kdeplot(data=data, x=f"{prop}_dft", y=f"{prop}_xtb", color='#212121', fill=False,levels=10, linewidths=linewidths,ax=ax_out)
    sns.kdeplot(data=data, x=f"{prop}_dft", y=f"{prop}_xtb", cmap = 'BuPu', fill=True,levels=levels,thresh=tresh,ax=ax)
    sns.kdeplot(data=data, x=f"{prop}_dft", y=f"{prop}_xtb", cmap = 'BuPu', fill=True,levels=levels,thresh=tresh,ax=ax_out)
    ax_out.set_xlim(AXIS_LIMITS[prop][0])
    # show only 5 ticks
    # y_ticks = range(AXIS_LIMITS[prop][1][0], AXIS_LIMITS[prop][1][1] + 1)
    # ax.yaxis.set_ticks(y_ticks)
    ax_out.set_ylim(AXIS_LIMITS[prop][1])
    ax_out.set_box_aspect(1)
    if limits:
        ax_out.set_xlim(AXIS_LIMITS[prop][0])
        ax_out.set_ylim(AXIS_LIMITS[prop][1])
    ax_out.set_title(f'{prop.upper()}')
    # force integer ticks
    ax_out.xaxis.set_major_locator(MaxNLocator(5,integer=True))
    ax_out.yaxis.set_major_locator(MaxNLocator(5,integer=True))
    plt.savefig(f"{name}.svg",dpi=300)
    plt.savefig(f"{name}.png",dpi=300,transparent=True)
    plt.close(fig)
    return ax

def plot_train_test_residual_inset(model, ax=None, name=None):
    # model.plot_results()
    print(model.X_train.columns)
    y_train_pred = model.predict(model.X_train)
    y_test_pred = model.predict(model.X_test)

    if ax:
        ax.scatter(model.y_train, y_train_pred, label='train', color='#01D5FF', alpha=1,s=1)
        ax.scatter(model.y_test, y_test_pred, label='test', color='#3D5999', alpha=1,s=1)
        # make axis square
        ax.set_xlim(ax.get_xlim())
        ax.set_ylim(ax.get_xlim())
        ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3",
                label="y=x",
                linewidth=1)
        
        ax.set_xlabel('True')
        ax.set_ylabel('Predicted')
        ax.legend()
        ax.set_title(f'{model.target}')
        # inset
        axins = ax.inset_axes([0.68, 0.02, 0.3, 0.3])
        axins.scatter(model.y_test, model.y_test - y_test_pred, label='test', color='#3D5999', alpha=1,s=1)
        # make axis ratio 1
        axins.set_box_aspect(1)
        # horizontal line at 0
        axins.axhline(y=0, color='k', linestyle='--', linewidth=1)
        # ax.set_xlabel('Predicted')
        # ax.set_ylabel('Residuals')
        # ax.legend()
        # hide x ticks
        axins.set_xticks([])
        # ax.set_title(f'{model.target}')
        # remove background
        axins.patch.set_visible(False)
        # hide legend
        ax.get_legend().remove()
        

    
    fig, ax = plt.subplots(figsize=(4,4), dpi=600)
    ax.scatter(model.y_train, y_train_pred, label='train', color='#01D5FF', alpha=1,s=1)
    ax.scatter(model.y_test, y_test_pred, label='test', color='#3D5999', alpha=1,s=1)
    # make axis square
    ax.set_xlim(ax.get_xlim())
    ax.set_ylim(ax.get_xlim())
    ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3",
            label="y=x",
            linewidth=1)
    
    ax.set_xlabel('True')
    ax.set_ylabel('Predicted')
    ax.legend()
    ax.set_title(f'{model.target}')
    # inset
    axins = ax.inset_axes([0.68, 0.02, 0.3, 0.3])
    axins.scatter(model.y_test, model.y_test - y_test_pred, label='test', color='#3D5999', alpha=1,s=1)
    # make axis ratio 1
    axins.set_box_aspect(1)
    # horizontal line at 0
    axins.axhline(y=0, color='k', linestyle='--', linewidth=1)
    # ax.set_xlabel('Predicted')
    # ax.set_ylabel('Residuals')
    # ax.legend()
    # hide x ticks
    axins.set_xticks([])
    # ax.set_title(f'{model.target}')
    # remove background
    axins.patch.set_visible(False)
    if name:
        plt.savefig(f"{name}.svg",dpi=300)
        plt.savefig(f"{name}.png",dpi=300,transparent=True)
    plt.close(fig)

    
    
    return fig, ax, axins

def galaxy_plot(sample, prop, alpha=0.5,
                name='galaxy-plot', limits=True,
                ax=None,
                **kwargs):
    """
    Plot a scatter plot of the given property. 

    Parameters
    ----------
    sample : pandas.DataFrame
        The sample to plot.
    prop : str
        The property to plot.
    alpha : float, optional
        The transparency of the points. The default is 0.5.
    name : str, optional
        The name of the plot. The default is 'galaxy-plot'.
    limits : bool, optional
        Whether to set the limits of the plot. The default is False. 
        If True, the limits are set to the ones in AXIS_LIMITS.
    ax : matplotlib.axes.Axes, optional
        The axes to plot on. The default is None.
    name : str, optional
        The name of the file to save the plot to (without extension). The default is None.
    **kwargs :
        Additional arguments to pass to sns.scatterplot.

    Returns
    -------
    None.
    """
    if ax:
        ax = sns.scatterplot(data = sample, x=f'{prop}_dft', y=f'{prop}_xtb',
                        alpha=alpha,
                        ax = ax,
                        # s=10,
                        # edge size
                        **kwargs)
        ax.set_box_aspect(1)
        if limits:
            ax.set_xlim(AXIS_LIMITS[prop][0])
            ax.set_ylim(AXIS_LIMITS[prop][1])
        # interger ticks
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        # hide axis labels
        ax.set_xlabel('')
        ax.set_ylabel('')
        ax.set_title(f'{prop.upper()}')
        # ax.set_title(f'{prop.upper()}')

    fig = plt.figure(figsize=(4,4),dpi=400)
    ax = sns.scatterplot(data = sample, x=f'{prop}_dft', y=f'{prop}_xtb',
                        alpha=alpha,
                        # s=10,
                        # edge size
                        **kwargs)
    ax.set_box_aspect(1)
    if limits:
        ax.set_xlim(AXIS_LIMITS[prop][0])
        ax.set_ylim(AXIS_LIMITS[prop][1])
    # show only 5 interger ticks
    # ax.xaxis.set_major_locator(MaxNLocator(5, ))
    # ax.yaxis.set_major_locator(MaxNLocator(5, ))
    # hide axis labels
    ax.set_xlabel('')
    ax.set_ylabel('')
    ax.set_title(f'{prop.upper()}')
    plt.savefig(f"{name}.svg",dpi=300)
    plt.savefig(f"{name}.png",dpi=400,transparent=True)
    plt.close(fig)
    return fig, ax