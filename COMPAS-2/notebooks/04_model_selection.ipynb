{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The COMPAS Project: A Computational Database of Polycyclic Aromatic Systems. Phase 2: _cata_-condensed Hetero-Polycyclic Aromatic Systems\n",
    "\n",
    "## Model Selection\n",
    "\n",
    "## Summary\n",
    "\n",
    "In this Jupyter notebook, we will explore the process of selecting a suitable model for regression analysis to predict Density Functional Theory (DFT) values using xTB values and molecular descriptors. The goal is to develop a regression model that accurately estimates DFT values based on the available xTB data and molecular descriptors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The autoreload extension is already loaded. To reload it, use:\n",
      "  %reload_ext autoreload\n"
     ]
    }
   ],
   "source": [
    "from rdkit import Chem\n",
    "from rdkit.Chem import PandasTools\n",
    "from rdkit.Chem import AllChem\n",
    "\n",
    "from pathlib import Path\n",
    "import json\n",
    "import re\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from datetime import datetime\n",
    "from tqdm import tqdm\n",
    "\n",
    "TIMESPAM = datetime.now().strftime(\"%y%m%d_%H%M%S\")\n",
    "\n",
    "import mols2grid\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "from utils.func import calc_gap, calc_aea_aip\n",
    "from utils.regression import prepare_model\n",
    "\n",
    "\n",
    "# training different models and comparing them\n",
    "from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet, ARDRegression\n",
    "from sklearn.ensemble import RandomForestRegressor\n",
    "from sklearn.svm import SVR\n",
    "from sklearn.neighbors import KNeighborsRegressor\n",
    "# import standar scaler\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "# import deepcopy\n",
    "from copy import deepcopy\n",
    "\n",
    "from utils.regression import Regressor, random_split, sabya_split\n",
    "# show all pandas columns in vscode\n",
    "pd.set_option('display.max_columns', None)\n",
    "\n",
    "# set autoreload\n",
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "\n",
    "DATA_FOLDER = Path('data')\n",
    "FIGURE_FOLDER = Path('figures')\n",
    "BENCHMARK_FOLDER = DATA_FOLDER / 'benchmark'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data Preparation\n",
    "The cells below show the data preparation steps, including data loading, handling missing values, and data preprocessing. We will also perform any necessary data transformations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read the compas-2x and compas-2D datasets ETA ~ <1 min\n",
    "df_xtb = pd.read_csv('../compas-2x.csv', index_col=0)\n",
    "df_dft = pd.read_csv('../compas-2d.csv', index_col=0)\n",
    "\n",
    "# merge the datasets\n",
    "data = pd.merge(df_xtb.drop(columns=['formula', 'inchi', 'smiles', \n",
    "                                     'h', 'c', 'b', 's', 'o', 'n',\n",
    "                                     'cyclobutadiene',\n",
    "       'pyrrole', 'borole', 'furan', 'thiophene', 'dhdiborinine', '14diborinine',\n",
    "       'pyrazine', 'pyridine', 'borinine', 'benzene'\n",
    "                                     ]),\n",
    "                df_dft, on=['name', 'charge'], suffixes=('_xtb', '_dft'))\n",
    "\n",
    "# make columns names shorter\n",
    "mapper = {'electron_affinity_xtb': 'aea_xtb',\n",
    "            'electron_affinity_dft': 'aea_dft',\n",
    "            'ionization_potential_xtb': 'aip_xtb',\n",
    "            'ionization_potential_dft': 'aip_dft',\n",
    "}\n",
    "data = data.rename(columns=mapper)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Choosing the best model\n",
    "Feature selection plays a crucial role in regression analysis. In this section, we will employ various feature selection techniques to identify the most relevant molecular descriptors that have the highest impact on predicting DFT values. We will evaluate the importance of each feature and select the subset of features that contribute the most to the regression model's performance.\n",
    "\n",
    "In this section, we will explore different regression models suitable for our problem, such as linear regression, support vector regression, random forest regression, and others. We will discuss the strengths and weaknesses of each model and compare their performance metrics to determine the most suitable model for our dataset.\n",
    "\n",
    "Once we have selected a regression model, we will evaluate its performance using appropriate metrics such as mean squared error (MSE), mean absolute error (MAE), and R-squared. We will also assess the model's ability to generalize by conducting cross-validation and analyzing the residuals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th>r2</th>\n",
       "      <th>rmse</th>\n",
       "      <th>mae</th>\n",
       "      <th>pearson</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>name</th>\n",
       "      <th>set</th>\n",
       "      <th>prop</th>\n",
       "      <th>scaler</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th rowspan=\"10\" valign=\"top\">ARDRegression</th>\n",
       "      <th rowspan=\"10\" valign=\"top\">atoms</th>\n",
       "      <th rowspan=\"2\" valign=\"top\">aea</th>\n",
       "      <th>None</th>\n",
       "      <td>0.743510</td>\n",
       "      <td>0.317200</td>\n",
       "      <td>0.230608</td>\n",
       "      <td>0.862276</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>StandardScaler</th>\n",
       "      <td>0.743510</td>\n",
       "      <td>0.317200</td>\n",
       "      <td>0.230607</td>\n",
       "      <td>0.862276</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">aip</th>\n",
       "      <th>None</th>\n",
       "      <td>0.826802</td>\n",
       "      <td>0.229826</td>\n",
       "      <td>0.152807</td>\n",
       "      <td>0.909310</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>StandardScaler</th>\n",
       "      <td>0.836311</td>\n",
       "      <td>0.223428</td>\n",
       "      <td>0.143696</td>\n",
       "      <td>0.914519</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">energy</th>\n",
       "      <th>None</th>\n",
       "      <td>1.000000</td>\n",
       "      <td>0.023136</td>\n",
       "      <td>0.018260</td>\n",
       "      <td>1.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>StandardScaler</th>\n",
       "      <td>1.000000</td>\n",
       "      <td>0.023136</td>\n",
       "      <td>0.018260</td>\n",
       "      <td>1.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">gap</th>\n",
       "      <th>None</th>\n",
       "      <td>0.885165</td>\n",
       "      <td>0.231528</td>\n",
       "      <td>0.159156</td>\n",
       "      <td>0.940833</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>StandardScaler</th>\n",
       "      <td>0.885165</td>\n",
       "      <td>0.231528</td>\n",
       "      <td>0.159156</td>\n",
       "      <td>0.940833</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">homo</th>\n",
       "      <th>None</th>\n",
       "      <td>0.892352</td>\n",
       "      <td>0.160355</td>\n",
       "      <td>0.108154</td>\n",
       "      <td>0.944648</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>StandardScaler</th>\n",
       "      <td>0.892856</td>\n",
       "      <td>0.159979</td>\n",
       "      <td>0.108549</td>\n",
       "      <td>0.944914</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                                 r2      rmse       mae  \\\n",
       "name          set   prop   scaler                                         \n",
       "ARDRegression atoms aea    None            0.743510  0.317200  0.230608   \n",
       "                           StandardScaler  0.743510  0.317200  0.230607   \n",
       "                    aip    None            0.826802  0.229826  0.152807   \n",
       "                           StandardScaler  0.836311  0.223428  0.143696   \n",
       "                    energy None            1.000000  0.023136  0.018260   \n",
       "                           StandardScaler  1.000000  0.023136  0.018260   \n",
       "                    gap    None            0.885165  0.231528  0.159156   \n",
       "                           StandardScaler  0.885165  0.231528  0.159156   \n",
       "                    homo   None            0.892352  0.160355  0.108154   \n",
       "                           StandardScaler  0.892856  0.159979  0.108549   \n",
       "\n",
       "                                            pearson  \n",
       "name          set   prop   scaler                    \n",
       "ARDRegression atoms aea    None            0.862276  \n",
       "                           StandardScaler  0.862276  \n",
       "                    aip    None            0.909310  \n",
       "                           StandardScaler  0.914519  \n",
       "                    energy None            1.000000  \n",
       "                           StandardScaler  1.000000  \n",
       "                    gap    None            0.940833  \n",
       "                           StandardScaler  0.940833  \n",
       "                    homo   None            0.944648  \n",
       "                           StandardScaler  0.944914  "
      ]
     },
     "execution_count": 77,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# ETA ~ 80 min\n",
    "props = ['aea', 'aip', 'homo', 'lumo', 'gap', 'energy']\n",
    "atoms = ['h', 'c', 'b', 's', 'o', 'n']\n",
    "bblocks = ['cyclobutadiene',\n",
    "       'pyrrole', 'borole', 'furan', 'thiophene', 'dhdiborinine', '14diborinine',\n",
    "       'pyrazine', 'pyridine', 'borinine', 'benzene']\n",
    "\n",
    "scalers = [None, StandardScaler()]\n",
    "props = ['aea', 'aip', 'homo', 'lumo', 'gap', 'energy']\n",
    "targets = {prop:f\"{prop}_dft\" for prop in props}\n",
    "\n",
    "targets = {prop:f\"{prop}_dft\" for prop in props}\n",
    "features_sets = {\"atoms\":{prop:[f\"{prop}_xtb\"]+atoms for prop in props},\n",
    "                \"bblocks\":{prop:[f\"{prop}_xtb\"]+bblocks for prop in props},\n",
    "                }\n",
    "\n",
    "models = [LinearRegression(),\n",
    "        Ridge(alpha=0.0001),\n",
    "        Ridge(alpha=0.00001), \n",
    "        Lasso(alpha=0.0001), \n",
    "        Lasso(alpha=0.00001), \n",
    "        ElasticNet(alpha=0.0001, l1_ratio=0.1), \n",
    "        ElasticNet(alpha=0.00001, l1_ratio=0.1), \n",
    "        RandomForestRegressor(), \n",
    "        KNeighborsRegressor(n_neighbors=5, metric='minkowski'),\n",
    "        KNeighborsRegressor(n_neighbors=5, metric='cosine'),\n",
    "        KNeighborsRegressor(n_neighbors=5, metric='canberra'),\n",
    "        KNeighborsRegressor(n_neighbors=5, metric='manhattan'),\n",
    "        KNeighborsRegressor(n_neighbors=5, metric='euclidean'),\n",
    "        KNeighborsRegressor(n_neighbors=5, metric='braycurtis'),\n",
    "        ARDRegression(),\n",
    "        SVR(kernel='rbf')\n",
    "        ]\n",
    "\n",
    "model_names = ['LinearRegression', \n",
    "                'Ridge4',\n",
    "                'Ridge5',\n",
    "                'Lasso4',\n",
    "                'Lasso5',\n",
    "                'ElasticNet4',\n",
    "                'ElasticNet5',\n",
    "                'RandomForestRegressor',\n",
    "                'KNeighborsRegressor-minkowski',\n",
    "                'KNeighborsRegressor-cosine',\n",
    "                'KNeighborsRegressor-canberra',\n",
    "                'KNeighborsRegressor-manhattan',\n",
    "                'KNeighborsRegressor-euclidean',\n",
    "                'KNeighborsRegressor-braycurtis',\n",
    "                'ARDRegression',\n",
    "                'SVR-rbf'\n",
    "            ]\n",
    "\n",
    "results = []\n",
    "for scaler in scalers:\n",
    "    trined_models = []\n",
    "    metrics = []\n",
    "    for skmode, name in zip(models, model_names):\n",
    "        for prop, target in targets.items():\n",
    "            for set, features in features_sets.items():\n",
    "                model, r2, rmse, mae, pearsonr = prepare_model(deepcopy(skmode), data, target, features[prop], scaler)\n",
    "                results.append([name, model, r2, rmse, mae, pearsonr, set, prop, str(scaler).split('(')[0]])\n",
    "results = pd.DataFrame(results,columns=['name','model','r2','rmse','mae','pearson','set','prop', 'scaler'])\n",
    "\n",
    "# save the regression results\n",
    "results.drop(columns=['model']).to_csv(DATA_FOLDER / f'regression-results-{TIMESPAM}.csv')\n",
    "results.to_csv(DATA_FOLDER / f'regression-results-{TIMESPAM}.pkl')\n",
    "\n",
    "results.groupby(['name','set','prop', 'scaler']).mean(numeric_only=True).sort_values(['name','set','prop','scaler']).head(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 78,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>name</th>\n",
       "      <th>set</th>\n",
       "      <th>prop</th>\n",
       "      <th>r2</th>\n",
       "      <th>rmse</th>\n",
       "      <th>mae</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>LinearRegression</td>\n",
       "      <td>atoms</td>\n",
       "      <td>aea</td>\n",
       "      <td>0.743511</td>\n",
       "      <td>0.317200</td>\n",
       "      <td>0.230629</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>LinearRegression</td>\n",
       "      <td>atoms</td>\n",
       "      <td>aip</td>\n",
       "      <td>0.836279</td>\n",
       "      <td>0.223449</td>\n",
       "      <td>0.143798</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>LinearRegression</td>\n",
       "      <td>atoms</td>\n",
       "      <td>energy</td>\n",
       "      <td>1.000000</td>\n",
       "      <td>0.023136</td>\n",
       "      <td>0.018260</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>LinearRegression</td>\n",
       "      <td>atoms</td>\n",
       "      <td>gap</td>\n",
       "      <td>0.885166</td>\n",
       "      <td>0.231527</td>\n",
       "      <td>0.159179</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>LinearRegression</td>\n",
       "      <td>atoms</td>\n",
       "      <td>homo</td>\n",
       "      <td>0.892856</td>\n",
       "      <td>0.159979</td>\n",
       "      <td>0.108550</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>LinearRegression</td>\n",
       "      <td>atoms</td>\n",
       "      <td>lumo</td>\n",
       "      <td>0.942309</td>\n",
       "      <td>0.140972</td>\n",
       "      <td>0.099583</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                name    set    prop        r2      rmse       mae\n",
       "0   LinearRegression  atoms     aea  0.743511  0.317200  0.230629\n",
       "2   LinearRegression  atoms     aip  0.836279  0.223449  0.143798\n",
       "10  LinearRegression  atoms  energy  1.000000  0.023136  0.018260\n",
       "8   LinearRegression  atoms     gap  0.885166  0.231527  0.159179\n",
       "4   LinearRegression  atoms    homo  0.892856  0.159979  0.108550\n",
       "6   LinearRegression  atoms    lumo  0.942309  0.140972  0.099583"
      ]
     },
     "execution_count": 78,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results.query('name == \"LinearRegression\" and scaler == \"None\"').query('set == \"atoms\"').drop(columns=['model']).sort_values(['prop'])[['name','set','prop','r2','rmse', 'mae']]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 79,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead tr th {\n",
       "        text-align: left;\n",
       "    }\n",
       "\n",
       "    .dataframe thead tr:last-of-type th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th colspan=\"2\" halign=\"left\">r2</th>\n",
       "      <th colspan=\"2\" halign=\"left\">rmse</th>\n",
       "      <th colspan=\"2\" halign=\"left\">mae</th>\n",
       "      <th colspan=\"2\" halign=\"left\">pearson</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th>mean</th>\n",
       "      <th>std</th>\n",
       "      <th>mean</th>\n",
       "      <th>std</th>\n",
       "      <th>mean</th>\n",
       "      <th>std</th>\n",
       "      <th>mean</th>\n",
       "      <th>std</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>name</th>\n",
       "      <th>set</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">ARDRegression</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.18</td>\n",
       "      <td>0.10</td>\n",
       "      <td>0.13</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.16</td>\n",
       "      <td>0.06</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">ElasticNet4</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.32</td>\n",
       "      <td>0.26</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.21</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.37</td>\n",
       "      <td>0.40</td>\n",
       "      <td>0.20</td>\n",
       "      <td>0.15</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">ElasticNet5</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.31</td>\n",
       "      <td>0.25</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.20</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.34</td>\n",
       "      <td>0.33</td>\n",
       "      <td>0.19</td>\n",
       "      <td>0.13</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">KNeighborsRegressor-braycurtis</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.76</td>\n",
       "      <td>0.12</td>\n",
       "      <td>8.75</td>\n",
       "      <td>20.67</td>\n",
       "      <td>3.09</td>\n",
       "      <td>7.06</td>\n",
       "      <td>0.87</td>\n",
       "      <td>0.06</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.79</td>\n",
       "      <td>0.08</td>\n",
       "      <td>14.49</td>\n",
       "      <td>34.80</td>\n",
       "      <td>7.37</td>\n",
       "      <td>17.57</td>\n",
       "      <td>0.89</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">KNeighborsRegressor-canberra</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.79</td>\n",
       "      <td>0.13</td>\n",
       "      <td>4.63</td>\n",
       "      <td>10.65</td>\n",
       "      <td>1.66</td>\n",
       "      <td>3.59</td>\n",
       "      <td>0.89</td>\n",
       "      <td>0.07</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.75</td>\n",
       "      <td>0.10</td>\n",
       "      <td>15.63</td>\n",
       "      <td>37.54</td>\n",
       "      <td>8.32</td>\n",
       "      <td>19.84</td>\n",
       "      <td>0.87</td>\n",
       "      <td>0.06</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">KNeighborsRegressor-cosine</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.70</td>\n",
       "      <td>0.13</td>\n",
       "      <td>18.01</td>\n",
       "      <td>43.30</td>\n",
       "      <td>6.66</td>\n",
       "      <td>15.75</td>\n",
       "      <td>0.83</td>\n",
       "      <td>0.08</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.70</td>\n",
       "      <td>0.08</td>\n",
       "      <td>23.30</td>\n",
       "      <td>56.28</td>\n",
       "      <td>12.40</td>\n",
       "      <td>29.81</td>\n",
       "      <td>0.84</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">KNeighborsRegressor-euclidean</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.77</td>\n",
       "      <td>0.11</td>\n",
       "      <td>9.50</td>\n",
       "      <td>22.53</td>\n",
       "      <td>3.37</td>\n",
       "      <td>7.74</td>\n",
       "      <td>0.88</td>\n",
       "      <td>0.06</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.80</td>\n",
       "      <td>0.08</td>\n",
       "      <td>14.54</td>\n",
       "      <td>34.95</td>\n",
       "      <td>7.49</td>\n",
       "      <td>17.86</td>\n",
       "      <td>0.89</td>\n",
       "      <td>0.04</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">KNeighborsRegressor-manhattan</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.76</td>\n",
       "      <td>0.12</td>\n",
       "      <td>8.81</td>\n",
       "      <td>20.83</td>\n",
       "      <td>3.13</td>\n",
       "      <td>7.15</td>\n",
       "      <td>0.87</td>\n",
       "      <td>0.06</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.79</td>\n",
       "      <td>0.08</td>\n",
       "      <td>14.50</td>\n",
       "      <td>34.82</td>\n",
       "      <td>7.37</td>\n",
       "      <td>17.57</td>\n",
       "      <td>0.89</td>\n",
       "      <td>0.04</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">KNeighborsRegressor-minkowski</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.77</td>\n",
       "      <td>0.11</td>\n",
       "      <td>9.50</td>\n",
       "      <td>22.53</td>\n",
       "      <td>3.37</td>\n",
       "      <td>7.74</td>\n",
       "      <td>0.88</td>\n",
       "      <td>0.06</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.80</td>\n",
       "      <td>0.08</td>\n",
       "      <td>14.54</td>\n",
       "      <td>34.95</td>\n",
       "      <td>7.49</td>\n",
       "      <td>17.86</td>\n",
       "      <td>0.89</td>\n",
       "      <td>0.04</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">Lasso4</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.31</td>\n",
       "      <td>0.25</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.20</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.34</td>\n",
       "      <td>0.32</td>\n",
       "      <td>0.19</td>\n",
       "      <td>0.13</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">Lasso5</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.31</td>\n",
       "      <td>0.25</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.20</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.34</td>\n",
       "      <td>0.32</td>\n",
       "      <td>0.19</td>\n",
       "      <td>0.13</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">LinearRegression</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.18</td>\n",
       "      <td>0.10</td>\n",
       "      <td>0.13</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.16</td>\n",
       "      <td>0.06</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">RandomForestRegressor</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.08</td>\n",
       "      <td>0.68</td>\n",
       "      <td>1.17</td>\n",
       "      <td>0.29</td>\n",
       "      <td>0.38</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.04</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.90</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.87</td>\n",
       "      <td>1.65</td>\n",
       "      <td>0.42</td>\n",
       "      <td>0.70</td>\n",
       "      <td>0.95</td>\n",
       "      <td>0.04</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">Ridge4</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.18</td>\n",
       "      <td>0.10</td>\n",
       "      <td>0.13</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.16</td>\n",
       "      <td>0.06</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">Ridge5</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.88</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.18</td>\n",
       "      <td>0.10</td>\n",
       "      <td>0.13</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.89</td>\n",
       "      <td>0.09</td>\n",
       "      <td>0.23</td>\n",
       "      <td>0.07</td>\n",
       "      <td>0.16</td>\n",
       "      <td>0.06</td>\n",
       "      <td>0.94</td>\n",
       "      <td>0.05</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th rowspan=\"2\" valign=\"top\">SVR-rbf</th>\n",
       "      <th>atoms</th>\n",
       "      <td>0.76</td>\n",
       "      <td>0.25</td>\n",
       "      <td>42.98</td>\n",
       "      <td>104.75</td>\n",
       "      <td>24.81</td>\n",
       "      <td>60.44</td>\n",
       "      <td>0.89</td>\n",
       "      <td>0.10</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>bblocks</th>\n",
       "      <td>0.78</td>\n",
       "      <td>0.26</td>\n",
       "      <td>43.09</td>\n",
       "      <td>105.06</td>\n",
       "      <td>25.35</td>\n",
       "      <td>61.78</td>\n",
       "      <td>0.90</td>\n",
       "      <td>0.11</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                          r2         rmse            mae  \\\n",
       "                                        mean   std   mean     std   mean   \n",
       "name                           set                                         \n",
       "ARDRegression                  atoms    0.88  0.09   0.18    0.10   0.13   \n",
       "                               bblocks  0.89  0.09   0.23    0.07   0.16   \n",
       "ElasticNet4                    atoms    0.88  0.09   0.32    0.26   0.23   \n",
       "                               bblocks  0.89  0.09   0.37    0.40   0.20   \n",
       "ElasticNet5                    atoms    0.88  0.09   0.31    0.25   0.23   \n",
       "                               bblocks  0.89  0.09   0.34    0.33   0.19   \n",
       "KNeighborsRegressor-braycurtis atoms    0.76  0.12   8.75   20.67   3.09   \n",
       "                               bblocks  0.79  0.08  14.49   34.80   7.37   \n",
       "KNeighborsRegressor-canberra   atoms    0.79  0.13   4.63   10.65   1.66   \n",
       "                               bblocks  0.75  0.10  15.63   37.54   8.32   \n",
       "KNeighborsRegressor-cosine     atoms    0.70  0.13  18.01   43.30   6.66   \n",
       "                               bblocks  0.70  0.08  23.30   56.28  12.40   \n",
       "KNeighborsRegressor-euclidean  atoms    0.77  0.11   9.50   22.53   3.37   \n",
       "                               bblocks  0.80  0.08  14.54   34.95   7.49   \n",
       "KNeighborsRegressor-manhattan  atoms    0.76  0.12   8.81   20.83   3.13   \n",
       "                               bblocks  0.79  0.08  14.50   34.82   7.37   \n",
       "KNeighborsRegressor-minkowski  atoms    0.77  0.11   9.50   22.53   3.37   \n",
       "                               bblocks  0.80  0.08  14.54   34.95   7.49   \n",
       "Lasso4                         atoms    0.88  0.09   0.31    0.25   0.23   \n",
       "                               bblocks  0.89  0.09   0.34    0.32   0.19   \n",
       "Lasso5                         atoms    0.88  0.09   0.31    0.25   0.23   \n",
       "                               bblocks  0.89  0.09   0.34    0.32   0.19   \n",
       "LinearRegression               atoms    0.88  0.09   0.18    0.10   0.13   \n",
       "                               bblocks  0.89  0.09   0.23    0.07   0.16   \n",
       "RandomForestRegressor          atoms    0.89  0.08   0.68    1.17   0.29   \n",
       "                               bblocks  0.90  0.07   0.87    1.65   0.42   \n",
       "Ridge4                         atoms    0.88  0.09   0.18    0.10   0.13   \n",
       "                               bblocks  0.89  0.09   0.23    0.07   0.16   \n",
       "Ridge5                         atoms    0.88  0.09   0.18    0.10   0.13   \n",
       "                               bblocks  0.89  0.09   0.23    0.07   0.16   \n",
       "SVR-rbf                        atoms    0.76  0.25  42.98  104.75  24.81   \n",
       "                               bblocks  0.78  0.26  43.09  105.06  25.35   \n",
       "\n",
       "                                              pearson        \n",
       "                                          std    mean   std  \n",
       "name                           set                           \n",
       "ARDRegression                  atoms     0.07    0.94  0.05  \n",
       "                               bblocks   0.06    0.94  0.05  \n",
       "ElasticNet4                    atoms     0.21    0.94  0.05  \n",
       "                               bblocks   0.15    0.94  0.05  \n",
       "ElasticNet5                    atoms     0.20    0.94  0.05  \n",
       "                               bblocks   0.13    0.94  0.05  \n",
       "KNeighborsRegressor-braycurtis atoms     7.06    0.87  0.06  \n",
       "                               bblocks  17.57    0.89  0.05  \n",
       "KNeighborsRegressor-canberra   atoms     3.59    0.89  0.07  \n",
       "                               bblocks  19.84    0.87  0.06  \n",
       "KNeighborsRegressor-cosine     atoms    15.75    0.83  0.08  \n",
       "                               bblocks  29.81    0.84  0.05  \n",
       "KNeighborsRegressor-euclidean  atoms     7.74    0.88  0.06  \n",
       "                               bblocks  17.86    0.89  0.04  \n",
       "KNeighborsRegressor-manhattan  atoms     7.15    0.87  0.06  \n",
       "                               bblocks  17.57    0.89  0.04  \n",
       "KNeighborsRegressor-minkowski  atoms     7.74    0.88  0.06  \n",
       "                               bblocks  17.86    0.89  0.04  \n",
       "Lasso4                         atoms     0.20    0.94  0.05  \n",
       "                               bblocks   0.13    0.94  0.05  \n",
       "Lasso5                         atoms     0.20    0.94  0.05  \n",
       "                               bblocks   0.13    0.94  0.05  \n",
       "LinearRegression               atoms     0.07    0.94  0.05  \n",
       "                               bblocks   0.06    0.94  0.05  \n",
       "RandomForestRegressor          atoms     0.38    0.94  0.04  \n",
       "                               bblocks   0.70    0.95  0.04  \n",
       "Ridge4                         atoms     0.07    0.94  0.05  \n",
       "                               bblocks   0.06    0.94  0.05  \n",
       "Ridge5                         atoms     0.07    0.94  0.05  \n",
       "                               bblocks   0.06    0.94  0.05  \n",
       "SVR-rbf                        atoms    60.44    0.89  0.10  \n",
       "                               bblocks  61.78    0.90  0.11  "
      ]
     },
     "execution_count": 79,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results_std = results.query('scaler == \"None\"').query('set == \"atoms\" or set == \"bblocks\"').groupby(['name', 'set']).std(numeric_only=True)\n",
    "results_mean = results.query('scaler == \"None\"').query('set == \"atoms\" or set == \"bblocks\"').groupby(['name', 'set']).mean(numeric_only=True)\n",
    "\n",
    "# make a dataframe with the mean+-std for each model\n",
    "results_mean_std = pd.concat([results_mean, results_std], axis=1)\n",
    "results_mean_std.columns = pd.MultiIndex.from_product([['mean', 'std'], results_mean.columns])\n",
    "results_mean_std = results_mean_std.swaplevel(axis=1).sort_index(axis=1)\n",
    "# R2 RMSE MAE pearson\n",
    "results_mean_std[['r2', 'rmse', 'mae', 'pearson']].round(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conclusion\n",
    "\n",
    "The models tested demonstrated similar outcomes overall, with slightly better mean absolute errors (MAEs) observed when training on building block information rather than molecular formula. The decision was made to use the Linear Regression model with xTB properties and atom counts for direct prediction of DFT properties due to its simplicity, transparency, and the negligible performance gain offered by more complex methods."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.15 ('chem_lab')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.15"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "665db3ddf615434586848dca6298abca2519eb8a0bf9392cdc2abc9a633fa199"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
