import pandas as pd
from scipy.stats import linregress

# Functions
def linear_regression(x,y,predict_x):
    """
    Calculate the linear regression of an x,y data pair and 
    returns the predicted y values of a given x vector.
    """

    # linear regression
    slope, intercept, r_value, p_value, std_err = linregress(x,y)

    # calculated y
    predict_y = intercept + slope * predict_x

    return predict_y

if '__main__' == __name__:

    # Variables
    # Dict of electronic properties we want to correct from xTB to dFT
    __electronic_properties__ = {
        'HOMO [eV]': 'HOMO_eV',
        'LUMO [eV]': 'LUMO_eV',
        'HOMO-LUMO gap [eV]': 'GAP_eV',
        'adiabatic Ionization Potential [eV]': 'aIP_eV',
        'adiabatic Electron Affinity [eV]': 'aEA_eV',
        'Relative energy [eV]': 'Erel_eV'
    }

    # Read in the data
    # xTB data
    compas1x = pd.read_csv('../compas-1x.csv')
    # DFT data (if want to correct to CAM-B3LYP/aug-cc-pVDZ)
    compas1D = pd.read_csv('../compas-1D_cam-b3lyp_aug-cc-pVDZ.csv') 

    # get the DFT-corrected data for COMPAS-1x
    compas1x = compas1D.sort_values('molecule')
    compas1D = compas1D.sort_values('molecule')
    molecules_dft = compas1D.molecule.tolist()
    compas1x_small = compas1x[compas1x.molecule.isin(molecules_dft)].copy()

    for name,prop in __electronic_properties__.items():
        compas1x[f'{prop}_corrected'] = linear_regression(
                                            compas1x_small[prop], # x = xTB data in same range as DFT data
                                            compas1D[prop],       # y = DFT data
                                            compas1x[prop]        # predict_x = all xTB data
                                        )