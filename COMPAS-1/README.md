# The COMPAS Project, Phase 1: *cata*-condensed Polybenzenoid Hydrocarbons
## Summary

This directory contains the COMPAS-1x and COMPAS-1D datasets, the first data drop of the COMPAS Project, which contains an exhaustive enumeration of the chemical space of *cata*-condensed polybenzenoid hydrocarbons comprising from 1 to 11 rings. 

![dataset overview](datasets_overview.png)

## Content
|  Data column header | COMPAS-1x | COMPAS-1D @ B3LYP/def2-SVP | COMPAS-1D @ CAM-B3LYP/aug-cc-pVDZ | Description |
|---------------------|-----------|----------------------------|----------------------------------|-------------|
| molecule | ✅ | ✅ | ✅ | Identifier of the molecule (includes the molecular formula and a serial number) |
| smiles | ✅ | ✅ | ✅ | SMILES string of the molecule |
| balaban_notation | ✅ | ✅ | ✅ | annulation sequence after Balaban et al. notation |
| augmented_lalas | ✅ | ✅ | ✅ | annulation sequence with angular directionality (aka augmented LALA string) |
| lalas | ✅ | ✅ | ✅ | annulation sequence without angular directionality (aka LALA string) |
| HOMO_eV | ✅ | ✅ | ✅ | HOMO energy in eV |
| LUMO_eV | ✅ | ✅ | ✅ | LUMO energy in eV |
| GAP_eV | ✅ | ✅ | ✅ | HOMO-LUMO gap in eV |
| Dipmom_Debye | ✅ | ✅ | ✅ | Dipole moment in Debye |
| E_SCF_eV | ❌ | ✅ | ✅ | SCF energy of the neutral molecule in eV |
| Etot_eV | ✅ | ✅ | ✅ | Total single-point energy of the neutral molecule in eV |
| Etot_pos_eV | ✅ | ✅ | ✅ | Total single-point energy of the positively charged molecule in eV |
| Etot_neg_eV | ✅ | ✅ | ✅ | Total single-point energy of the negatively charged molecule in eV |
| ZPE_eV | ✅ | ❌ | ❌ | Zero-point energy of the neutral molecule in eV |
| ZPE_pos_eV | ✅ | ❌ | ❌ | Zero-point energy of the positively charged molecule in eV |
| ZPE_neg_eV | ✅ | ❌ | ❌ | Zero-point energy of the negatively charged molecule in eV |
| aEA_eV | ✅ | ✅ | ✅ | Adiabatic electron affinity in eV |
| aIP_eV | ✅ | ✅ | ✅ | Adiabatic ionization potential in eV |
| D3_disp_corr_eV | ❌ | ✅ | ✅ | D3 dispersion corrections in eV |
| D4_disp_corr_eV | ✅ | ❌ | ❌ | D4 dispersion corrections in eV |
| n_rings | ✅ | ✅ | ✅ | Number of rings |
| Erel_eV | ✅ | ✅ | ✅ | Relative single-point energy of the neutral molecule (with respect to the lowest-energy isomer) in eV |
| Erel_SCF_eV | ❌ | ✅ | ✅ | Relative SCF energy of the neutral molecule (with respect to the lowest-energy isomer) in eV |
| NFOD | ✅ | ❌ | ✅ | Integral of the fractional occupation weighted density | 
| y_value | ❌ | ✅ | ✅ | Quantitative description of the open-shell singlet diradical character |
| HOMO_eV_corrected | ✅ | ❌ | ❌ | xTB-to-DFT-corrected HOMO energy in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| LUMO_eV_corrected | ✅ | ❌ | ❌ | xTB-to-DFT-corrected LUMO energy in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| GAP_eV_corrected | ✅ | ❌ | ❌ | xTB-to-DFT-corrected HOMO-LUMO gap energy in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| aIP_eV_corrected | ✅ | ❌ | ❌ | xTB-to-DFT-corrected adiabatic ionization potential in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| aEA_eV_corrected | ✅ | ❌ | ❌ | xTB-to-DFT-corrected adiabatic electron affinity in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|

## Folder structure

```
├── compas-1x.csv
├── compas-1x.tar.gz
├── compas-1D_b3lyp_def2-svp.csv
├── compas-1D_b3lyp_def2-svp.tar.gz
├── compas-1D-cam-b3lyp_cc-aug-pvdz.csv
├── compas-1D-cam-b3lyp_def2-svp.tar.gz
│
└── scripts
    └── xtb2dft.py

```
The `compas-1x.csv`, `compas-1D_b3lyp_def2-svp.csv`, and `compas-1D_cam-b3lyp_aug-cc-pvdz.csv` files contain the electronic properties of the molecules.
The `compas-1x.tar.gz`, `compas-1D_b3lyp_def2-svp.tar.gz`, and `compas-1D_cam-b3lyp_def2-svp.tar.gz` files contain the optimized geometries of the molecules, in the respective levels of theory.
The `scripts` folder contains a python script, `xtb2dft.py` to calculate the xTB-to-DFT-corrected values.

## How to cite this dataset
If you use the COMPAS-1 datasets or any part of them, please cite the following:

> A. Wahab, L. Pfuderer, E. Paenurk, and R. Gershoni-Poranne, The COMPAS Project: A Computational Database of Polycyclic Aromatic Systems. Phase 1: cata-condensed Polybenzenoid Hydrocarbons, DOI: 10.1021/acs.jcim.2c00503

## Support
For support or to report any issues with the COMPAS database, please contact: porannegroup /at/ technion.ac.il

## Authors and acknowledgment
The COMPAS database was conceptualized by Prof. Renana Gershoni-Poranne and is constructed under her supervision. The following people contributed to data generation, curation, and organization of the COMPAS-1 datasets: 
1. Dr. Alexandra Wahab (ETH Zurich)
2. Ms. Lara Pfuderer (ETH Zurich)
3. Dr. Eno Paenurk (ETH Zurich)

The invaluable assistance of the following people is gratefully acknowledged: Prof. Dr. Peter Chen, Dr. Alexandra Tsybizova. 

In addition, the financial support of the Branco Weiss Fellowship is acknowledged.

## License
The COMPAS database is provided free-of-charge and is intended to be a resource for the scientific community.
It is licensed under a CC-BY-NC-SA license. 

