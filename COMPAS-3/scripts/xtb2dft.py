import pandas as pd
from scipy.stats import linregress

# Functions
def linear_regression(x,y,predict_x):
    """
    Calculate the linear regression of an x,y data pair and 
    returns the predicted y values of a given x vector.
    """

    # linear regression
    slope, intercept, r_value, p_value, std_err = linregress(x,y)

    # calculated y
    predict_y = intercept + slope * predict_x

    return predict_y

if '__main__' == __name__:

    # Variables
    # Dict of electronic properties we want to correct from xTB to dFT
    __electronic_properties__ = {
        'HOMO [eV]': 'HOMO_eV',
        'LUMO [eV]': 'LUMO_eV',
        'HOMO-LUMO gap [eV]': 'GAP_eV',
        'adiabatic Ionization Potential [eV]': 'aIP_eV',
        'adiabatic Electron Affinity [eV]': 'aEA_eV',
        'Relative energy [eV]': 'Erel_eV'
    }

    # Read in the data
    # xTB data
    compas3x = pd.read_csv('../compas-3x.csv')
    # DFT data
    compas3D = pd.read_csv('../compas-3D.csv') 

    # get the DFT-corrected data for COMPAS-1x
    compas3x = compas3D.sort_values('molecule')
    compas3D = compas3D.sort_values('molecule')
    molecules_dft = compas3D.molecule.tolist()
    compas3x_small = compas3x[compas3x.molecule.isin(molecules_dft)].copy()

    for name,prop in __electronic_properties__.items():
        compas3x[f'{prop}_corrected'] = linear_regression(
                                            compas3x_small[prop], # x = xTB data in same range as DFT data
                                            compas3D[prop],       # y = DFT data
                                            compas3x[prop]        # predict_x = all xTB data
                                        )