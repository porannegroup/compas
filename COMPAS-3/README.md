# The COMPAS Project, Phase 3: *peri*-condensed Polybenzenoid Hydrocarbons
## Summary

This directory contains the COMPAS-3x and COMPAS-3D data sets, an expansion of the COMPAS Project to the chemical space of *peri*-condensed polybenzenoid hydrocarbons. 

## Content
![dataset overview](data_overview.png)

## Content
|  Data column header | COMPAS-3x | COMPAS-3D | Description |
|---------------------|-----------|-----------|-------------|
| molecule | ✅ | ✅ | Identifier of the molecule (includes the molecular formula and a serial number) |
| smiles | ✅ | ✅ | SMILES string of the molecule |
| HOMO_eV | ✅ | ✅ | HOMO energy in eV |
| LUMO_eV | ✅ | ✅ | LUMO energy in eV |
| GAP_eV | ✅ | ✅ | HOMO-LUMO gap in eV |
| Dipmom_Debye | ✅ | ✅ | Dipole moment in Debye |
| E_SCF_eV | ❌ | ✅ | SCF energy of the neutral molecule in eV |
| Etot_eV | ✅ | ✅ | Total single-point energy of the neutral molecule in eV |
| Etot_pos_eV | ✅ | ✅ | Total single-point energy of the positively charged molecule in eV |
| Etot_neg_eV | ✅ | ✅ | Total single-point energy of the negatively charged molecule in eV |
| ZPE_eV | ✅ | ❌ | Zero-point energy of the neutral molecule in eV |
| ZPE_pos_eV | ✅ | ❌ | Zero-point energy of the positively charged molecule in eV |
| ZPE_neg_eV | ✅ | ❌ | Zero-point energy of the negatively charged molecule in eV |
| aEA_eV | ✅ | ✅ | Adiabatic electron affinity in eV |
| aIP_eV | ✅ | ✅ | Adiabatic ionization potential in eV |
| D3_disp_corr_eV | ❌ | ✅ | D3 dispersion corrections in eV |
| D4_disp_corr_eV | ✅ | ❌ | D4 dispersion corrections in eV |
| n_rings | ✅ | ✅ | Number of rings |
| Erel_eV | ✅ | ✅ | Relative single-point energy of the neutral molecule (with respect to the lowest-energy isomer) in eV |
| Erel_SCF_eV | ❌ | ✅ | Relative SCF energy of the neutral molecule (with respect to the lowest-energy isomer) in eV |
| NFOD | ✅ | ✅ | Integral of the fractional occupation weighted density | 
| y_value | ❌ | ✅ | Quantitative description of the open-shell singlet diradical character |
| HOMO_eV_corrected | ✅ | ❌ | xTB-to-DFT-corrected HOMO energy in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| LUMO_eV_corrected | ✅ | ❌ | xTB-to-DFT-corrected LUMO energy in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| GAP_eV_corrected | ✅ | ❌ | xTB-to-DFT-corrected HOMO-LUMO gap energy in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| aIP_eV_corrected | ✅ | ❌ | xTB-to-DFT-corrected adiabatic ionization potential in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|
| aEA_eV_corrected | ✅ | ❌ | xTB-to-DFT-corrected adiabatic electron affinity in eV (@ CAM-B3LYP/aug-cc-pVDZ level)|

## Folder structure

```
├── compas-3x.csv
├── compas-3x.tar.gz
├── compas-3D.csv
├── compas-3D.tar.gz
│
└── scripts
    └── xtb2dft.py
```
The `compas-3x.csv` and `compas-3D.csv` files contain the electronic properties of the molecules.
The `compas-3x.tar.gz` and `compas-3D.tar.gz` files contain the optimized geometries of the molecules, in the respective levels of theory.
The `scripts` folder contains a python script, `xtb2dft.py` to calculate the xTB-to-DFT-corrected values.

## How to cite this dataset
If you use the COMPAS-3 data sets or any part of them, please cite the following:

> A. Wahab and R. Gershoni-Poranne, COMPAS-3: A Dataset of Peri-Condensed Polybenzenoid Hydrocarbons, Phys. Chem. Chem. Phys. 2024, 26 (21), 15344–15357, DOI: 10.1039/D4CP01027B

## Support
For support or to report any issues with the COMPAS database, please contact: porannegroup /at/ technion.ac.il

## Authors and acknowledgment
The COMPAS database was conceptualized by Prof. Renana Gershoni-Poranne and is constructed under her supervision. The following people contributed to data generation, curation, and organization of the COMPAS-3 datasets: 
1. Dr. Alexandra Wahab (ETH Zurich)

The invaluable assistance of the following people is gratefully acknowledged: Prof. Dr. Peter Chen, Dr. Eno Paenurk. 

In addition, the financial support of the Branco Weiss Fellowship is acknowledged.

## License
The COMPAS database is provided free-of-charge and is intended to be a resource for the scientific community.
It is licensed under a CC-BY-NC-SA license. 

